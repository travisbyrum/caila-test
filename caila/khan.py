# -*- encoding: utf-8 -*-

"""
    caila.khan
    ~~~~~~~~~~
    Scraping classes and functions for khan academy.
"""

import os
import logging
from queue import Queue

from .resource import ApiResource

logger = logging.getLogger(__name__)


class TopicTreeResource(ApiResource):
    """Topic api resource class."""

    BASE_URL = 'http://www.khanacademy.org'

    def __init__(self):
        super().__init__(
            url=os.path.join(
                type(self).BASE_URL,
                'api/v1/topictree'
            )
        )

    def get(self):
        return self._get().json()


def khan_info(node):
    """Convert khan topic tree node into dict."""

    return dict(
        title=node.get('title', ''),
        node_id=node.get('id'),
        web_url=node.get('web_url', ''),
        twitter_url=node.get('twitter_url', ''),
        facebook_url=node.get('facebook_url', ''),
        slug=node.get('node_slug', ''),
        kind=node.get('kind', ''),
        youtube_url=node.get('youtube_id', ''),
        creation=node.get('creation_date', '')
    )


def khan_bfs_search(root, max_iterations=10):
    """Breadth first search of khan topic tree."""

    nodes = Queue()
    nodes.put(root)
    visited = []
    info_out = []
    node_count = 1

    while not nodes.empty():
        if node_count > max_iterations:
            break

        current_node = nodes.get()
        node_id = current_node.get('id')
        logging.info('scraping node ' + node_id)

        if node_id in visited:
            continue

        visited.append(node_id)
        info_out.append(khan_info(current_node))
        node_count += 1

        for child in current_node.get('children', '') or []:
            nodes.put(child)

    return info_out
