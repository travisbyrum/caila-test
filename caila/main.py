# -*- encoding: utf-8 -*-

"""
    caila.main
    ~~~~~~~~~~
    Contains package entry point
"""

import os
import csv
import logging
import argparse

from .khan import khan_bfs_search, TopicTreeResource


def khan(**kwargs):
    """Main method for kahn scraping."""

    tree = TopicTreeResource().get()
    info = khan_bfs_search(tree, max_iterations=kwargs.get('max_iterations'))

    with open(kwargs.get('output'), 'w') as csv_file:
        writer = csv.DictWriter(csv_file, info[0].keys())
        writer.writeheader()
        writer.writerows(info)


def main(log_level=os.getenv('LOG_LEVEL', 'INFO')):
    """Main method defining package entry point."""

    log_level = dict(
        NOTSET=logging.NOTSET,
        DEBUG=logging.DEBUG,
        INFO=logging.INFO,
        WARNING=logging.WARNING,
        ERROR=logging.ERROR,
        CRITICAL=logging.CRITICAL,
    ).get(log_level.upper())

    logging.basicConfig(level=log_level)

    parser = argparse.ArgumentParser(description='Scraper CLI')
    subparsers = parser.add_subparsers(
        title='sub-commands',
        help='Choose sub-command'
    )
    parser_khan = subparsers.add_parser('khan', help='Khan scraping command')
    parser_khan.add_argument(
        '--max-iterations',
        type=int,
        help='Max depth to search',
        default=10
    )
    parser_khan.add_argument(
        '--output', '-o',
        help='File path for output.'
    )
    parser_khan.set_defaults(func=khan)

    args = parser.parse_args()
    args.func(**vars(args))
