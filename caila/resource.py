# -*- encoding: utf-8 -*-

"""
    caila.resource
    ~~~~~~~~~~~~~~
    Define api resources.
"""

import requests
from requests import HTTPError


class ApiResource(object):
    """Abstract class used to define api resources."""

    def __init__(self, url):
        self.url = url
        self.session = requests.Session()

    def _method(self, method, headers=None, data=None, files=None):
        """Internal base methods used to construct all requests."""

        response = self.session.request(
            method.upper(),
            self.url,
            headers=headers,
            data=data,
            files=files
        )

        if not response.ok:
            raise HTTPError

        return response

    def _get(self, headers=None, data=None, files=None):
        """Api get method."""

        return self._method('GET', headers=headers, data=data, files=files)

    def _post(self, headers=None, data=None, files=None):
        """Api post method."""

        return self._method('POST', headers=headers, data=data, files=files)

    def _put(self, headers=None, data=None, files=None):
        """Api put method."""

        return self._method('PUT', headers=headers, data=data, files=files)

    def get(self):
        """To be implemented public get method."""

        raise NotImplementedError
