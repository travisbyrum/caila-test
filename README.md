# Caila Web Scraping Rest

## (A)

- Use the Khan Academy API to pull content from [khan academy](https://api-explorer.khanacademy.org/)
- Present the data for list of courses in Khan Academy. If you can have the link to the source (Youtube) video, title, description, and other attributes
- Once you have done this, send us an email with a (1) CSV output file and (2) a link to your github code and data to sergio@getcaila.com

## (B)

- Use the Youtube API to pull content from Youtube (also see [docs](https://developers.google.com/youtube/v3/sample_requests))
- Present the data for the [Udacity Playlists](https://www.youtube.com/user/Udacity/playlists) If it takes extensive time, just do the Building your Business as a Tech Entrepreneur List. The attributes/features to capture are: title, description, creator, video URL, and type of content.

Once you have done this, send us an email with a (1) CSV output file and (2) a link to your github code and data to sergio@getcaila.com

## (Bonus - optional)

- Use the REST API to pull content from iTunes (also see <https://rss.itunes.apple.com/en-us>)
- Present the data for the top 100 books, audiobooks, podcasts, iTunesU courses. The attributes/features to capture are: title, description, creator, content URL, and type of content.

## Your 3rd challenge:

- Scrape the data for courses at [Coursera](https://www.coursera.org/browse?languages=en). The attributes/features to capture are: title, description, course URL, commitment, language (and other attributes on the page).
- You can build a spider using the Scrapy or Beautiful Soup framework using Python. The bot will crawl specific pages on a site to extract certain data. This is a [framework](http://scrapy.org/) you can also use [Beautiful Soup] (http://www.pythonforbeginners.com/beautifulsoup/beautifulsoup-4-python)
- Here are some example files of a few spiders that our developer built to crawl Bunker Hill Community College, Suffolk University, and Boston University [crawler](https://github.com/rangertaha/edu-spiders)