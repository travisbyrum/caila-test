FROM ubuntu:16.04

MAINTAINER Travis Byrum "travis.tbyrum@gmail.com"

RUN apt-get update \
  && apt-get install -y wget python3-pip python3-dev libfontconfig1 \
  && apt-get update -y \
  && apt-get upgrade -y \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip \
  && pip3 install virtualenv

WORKDIR /caila

ADD . /caila

RUN virtualenv -p python3 ENV \
  && make install

#CMD [ "caila" ]
