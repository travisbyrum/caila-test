# -*- encoding: utf-8 -*-

import ast
import re
from setuptools import setup, find_packages

_version_re = re.compile(r'__version__\s+=\s+(.*)')

with open('caila/__init__.py', 'rb') as f:
    version = str(
        ast.literal_eval(
            _version_re.search(
                f.read().decode('utf-8')).group(1)
        )
    )

setup(
    name='caila',
    version=version,
    description='Caila Web test',
    author='Travis Byrum',
    author_email='travis.tbyrum@gmail.com',
    license='MIT',
    include_package_data=True,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'caila = caila.main:main'
        ]
    }
)
