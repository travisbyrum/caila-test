MAKEFLAGS += --silent
PYTHON := ENV/bin/python
PIP := ENV/bin/pip
LINT := $(shell which pylint)
NAME := $(shell basename $(CURDIR))

all: help

help:
	echo "install      - install package"
	echo "uninstall    - uninstall package"
	echo "dev-install  - install package for development"
	echo "clean-build  - remove build artifacts"
	echo "clean-pyc    - remove python file artifacts"
	echo "pylint       - run pylint"
	echo "test         - run unit tests"

install:
	$(PIP) install -r requirements.txt
	$(PIP) install .

dev-install:
	$(PIP) install -e .

uninstall:
	$(PIP) uninstall .

clean: clean-build clean-pyc

clean-build:
	rm -fr asr/build/
	rm -fr asr/dist/
	rm -fr *.egg-info
	rm -fr .sass-cache/

clean-pyc:
	find asr -name '*.pyc' -exec rm -f {} +
	find asr -name '*.pyo' -exec rm -f {} +

pylint:
	$(LINT) -rn .

test:
	$(PYTHON) -m unittest discover

docker-build:
	docker build -t $(NAME) .

.PHONY: all help install dev-install uninstall \
	clean clean-build clean-pyc test pylint watch \
	docker-build