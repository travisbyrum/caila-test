# -*- encoding: utf-8 -*-

"""
    tests.test_khan
    ~~~~~~~~~~~~~~~
    Unit tests for Khan academy functions and classes.
"""

import unittest

from caila.khan import TopicTreeResource, khan_info, khan_bfs_search


class TestKhan(unittest.TestCase):

    def setUp(self):
        self.tree = TopicTreeResource().get()

    def test_topic_tree(self):
        self.assertIsInstance(self.tree, dict)

        for name in ['id', 'node_slug', 'child_data', 'children']:
            self.assertTrue(name in self.tree.keys())

    def test_khan_node(self):
        node = khan_info(self.tree)
        self.assertIsInstance(node, dict)

        for name in ['title', 'creation', 'web_url', 'youtube_url',
                     'twitter_url', 'node_id', 'facebook_url', 'slug', 'kind']:
            self.assertTrue(name in node.keys())

    def test_khan_bfs(self):
        info = khan_bfs_search(self.tree, max_iterations=10)

        self.assertIsInstance(info, list)
        self.assertTrue(len(info), 10)


if __name__ == '__main__':
    unittest.main()
